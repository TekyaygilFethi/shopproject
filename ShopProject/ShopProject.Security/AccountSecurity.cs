﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Business.UnitOfWorkFolder;
using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace ShopProject.Security
{
    public class AccountSecurity
    {
        static IRepository<User> userRepository=new Repository<User>(new Data.DbContextFolder.ShopProjectDbContext());

        public const string TokenPassword= "19952807";


        public static string GenerateSalt()
        {
            return Crypto.GenerateSalt();
        }

        public static string GetHashSaltPassword(string password, string salt)
        {
            var passwordLast = password + salt;
            
            return Crypto.HashPassword(passwordLast);
        }

        public static bool IsUserExists(string email,string password)
        {
            User _user = userRepository.SingleGetBy(w => w.Email == email);

            if (_user != null)
            {
                var userEnteredPassword = password + _user.Salt;
                return Crypto.VerifyHashedPassword(_user.Password, userEnteredPassword);
            }
            else
                return false;
        }


    }
}
