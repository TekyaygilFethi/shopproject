﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.Filters;
using ShopProject.MVC.FormDatas;
using ShopProject.MVC.ModelBinders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.Controllers
{
    
    public class ProductController : BaseController
    {
        IRepository<Product> productRepository = uow.GetRepository<Product>();
        IRepository<Category> categoryRepository = uow.GetRepository<Category>();
        IRepository<ProductQuantity> productQuantityRepository = uow.GetRepository<ProductQuantity>();
        IRepository<Purchase> purchaseRepository = uow.GetRepository<Purchase>();
        IRepository<Address> addressRepository = uow.GetRepository<Address>();
        IRepository<User> userRepository = uow.GetRepository<User>();
        IRepository<ShoppingCart> shoppingCartRepository = uow.GetRepository<ShoppingCart>();

        [AdminRoleAuthentication]
        public ActionResult CreateProduct()
        {
            TempData["Categories"] = categoryRepository.GetAll().Select(s => new SelectListItem
            {
                Value = s.ID.ToString(),
                Text = s.Name,
                Selected = true,
            }).ToList();
            return View();
        }

        [AdminRoleAuthentication]
        [HttpPost]
        public ActionResult CreateProduct([ModelBinder(typeof(CreateProductModelBinder))] Product product)
        {
            if (ModelState.IsValid)
            {
                productRepository.Add(product);
                uow.SaveChanges();
                TempData["Status"] = "success";
                TempData["Message"] = "Record added successfully!";
            }
            else
            {
                TempData["Status"] = "danger";
                TempData["Message"] = "Record could not added successfully!!!";
            }
            return View();
        }

        public ActionResult ProductList(string filterString,string categoryName,string sortMethod)
        {
            if (String.IsNullOrEmpty(filterString))
            {
                TempData["CategoryName"] = categoryName;

                IEnumerable<Product> Prods = new List<Product>();
                if (String.IsNullOrEmpty(categoryName))
                {
                    Prods = productRepository.GetAll();
                }
                else
                {
                    Prods = productRepository.GetBy(w => w.Category.Name == categoryName);
                }

                switch (sortMethod)
                {
                    case "azName":
                        Prods = Prods.OrderBy(w => w.Name);
                        break;

                    case "zaName":
                        Prods = Prods.OrderByDescending(w => w.Name);
                        break;

                    case "lhPrice":
                        Prods = Prods.OrderBy(o => o.Price);
                        break;

                    case "hlPrice":
                        Prods = Prods.OrderByDescending(o => o.Price);
                        break;
                }

                ICollection<ProductFormData> pfdList = new List<ProductFormData>();

                foreach (var production in Prods)
                {
                    ProductFormData pfd = new ProductFormData();
                    pfd.ID = production.ID;
                    pfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(production.ProductImage);
                    pfdList.Add(pfd);
                }

                ProductListFormData plfm = new ProductListFormData
                {
                    Categories = categoryRepository.GetAll(),
                    Products = Prods,
                    ProductFormDatas = pfdList
                };
                return View(plfm);
            }
            else
            {
                TempData["CategoryName"] = categoryName;

                IEnumerable<Product> Prods;
                if (categoryName != null && categoryName!="")
                {
                     Prods = productRepository.GetBy(w => w.Name.ToLower().Contains(filterString.ToLower()) && w.Category.Name == categoryName).ToList();
                }
                else
                {
                     Prods = productRepository.GetBy(w => w.Name.ToLower().Contains(filterString.ToLower())).ToList();
                }
                switch (sortMethod)
                {
                    case "azName":
                        Prods = Prods.OrderBy(w => w.Name);
                        break;

                    case "zaName":
                        Prods = Prods.OrderByDescending(w => w.Name);
                        break;

                    case "lhPrice":
                        Prods = Prods.OrderBy(o => o.Price);
                        break;

                    case "hlPrice":
                        Prods = Prods.OrderByDescending(o => o.Price);
                        break;
                }

                ICollection<ProductFormData> pfdList = new List<ProductFormData>();

                foreach (var production in Prods)
                {
                    ProductFormData pfd = new ProductFormData();
                    pfd.ID = production.ID;
                    pfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(production.ProductImage);
                    pfdList.Add(pfd);
                }

                ProductListFormData plfm = new ProductListFormData
                {
                    Categories = categoryRepository.GetAll(),
                    Products = Prods,
                    ProductFormDatas = pfdList
                };
                return View(plfm);

            }


            
        }

        public ActionResult ProductDetails(int ID)
        {
            Product selectedProduct = productRepository.GetById(ID);

            ICollection<Product> AllProductsInCategory = productRepository.GetBy(w => w.CategoryID == selectedProduct.CategoryID && w.ID != ID).ToList();
            ICollection<ProductFormData> pfdList = new List<ProductFormData>();

            ProductFormData pfdInit = new ProductFormData();
            pfdInit.ID = selectedProduct.ID;
            pfdInit.Base64Image = "data:image/png;base64," + Convert.ToBase64String(selectedProduct.ProductImage);
            pfdList.Add(pfdInit);

            ICollection<Product> finalizedProducts = new List<Product>();

            for (int i = 0; i < 4; i++) //get 4 random if there are 4 else return all existed
            {
                if (AllProductsInCategory.Count() != 0)
                {
                    Product randSelectedProduct = AllProductsInCategory.ElementAt(FakeData.NumberData.GetNumber(0, AllProductsInCategory.Count() - 1));

                    if (randSelectedProduct != null)
                    {
                        finalizedProducts.Add(randSelectedProduct);
                        AllProductsInCategory.Remove(randSelectedProduct);
                    }
                }
                else
                    break;
            }

            foreach (var production in finalizedProducts)
            {
                ProductFormData pfd = new ProductFormData();
                pfd.ID = production.ID;
                pfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(production.ProductImage);
                pfdList.Add(pfd);
            }

            return View(new ProductDetailsFormData() { Categories = categoryRepository.GetAll(), Product = selectedProduct, ProductFormDatas = pfdList, SameCategoryProducts = finalizedProducts });
        }

        [HttpPost]
        public JsonResult AddProductToCart(int ID)
        {
            User currentUser = Session["User"] as User;
            ShoppingCart currentShoppingCart = currentUser.ShoppingCart;

            if (currentShoppingCart.ProductQuantities.Where(w => w.ProductID == ID).Count() == 0)
            {
                Product AddedProduct = productRepository.GetById(ID);

                ProductQuantity newProductQuantity = new ProductQuantity();
                newProductQuantity.Product = AddedProduct;
                newProductQuantity.Quantity = 1;
                newProductQuantity.ShoppingCart = currentShoppingCart;

                currentShoppingCart.ProductQuantities.Add(newProductQuantity);
                currentShoppingCart.TotalPrice = SetTotalPrice(currentShoppingCart);

                productRepository.Attach(AddedProduct);


                productQuantityRepository.Add(newProductQuantity);
                try
                {
                    uow.SaveChanges();
                }
                catch (Exception ex) { }
            }
            else
            {
                currentShoppingCart.ProductQuantities.SingleOrDefault(w => w.ProductID == ID).Quantity++;
            }

            return Json("");
        }

        public double SetTotalPrice(ShoppingCart shoppingCart)
        {
            double totalPrice = 0;
            foreach (var productQuantity in shoppingCart.ProductQuantities)
            {
                totalPrice += productQuantity.Product.Price * productQuantity.Quantity;
            }
            return totalPrice;
        }

        public ActionResult DisplayShoppingCart()
        {
            return View((Session["User"] as User).ShoppingCart);
        }

        [HttpPost]
        public JsonResult GetAddressDescription(int addressID)
        {
            if (addressID == -1)
            {
                return Json("Please enter your address");
            }
            else
            {
                string addressDesc = addressRepository.SingleGetBy(w => w.ID == addressID).Description;
                return Json(addressDesc);
            }
        }

        public ActionResult PurchaseShoppingCart() //Sadece Adresi vereceksin buradan
        {
            User currentUser = Session["User"] as User;

            if (currentUser != null)
            {
                #region Address 

                ICollection<Address> addressList = new List<Address>();

                addressList.Add(new Address { ID = -1, AddressInfo = "New Address", Description = "", User = null });
                if (currentUser.Addresses != null)
                {
                    foreach (var address in currentUser.Addresses)
                    {
                        addressList.Add(address);
                    }
                }
                TempData["Addresses"] = addressList.Select(s => new SelectListItem
                {
                    Text = s.AddressInfo,
                    Value = s.ID.ToString(),
                    Selected = true
                });
                #endregion

                ShoppingCartFormData scfd = null;
                //if (ID != null)
                //{
                ShoppingCart currentShoppingCart = shoppingCartRepository.SingleGetBy(w => w.ID == currentUser.ID);
                ICollection<ProductFormData> pfdList = new List<ProductFormData>();

                foreach (var productQuantity in currentShoppingCart.ProductQuantities)
                {

                    ProductFormData pfd = new ProductFormData();
                    pfd.ID = productQuantity.ProductID;
                    pfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(productQuantity.Product.ProductImage);
                    pfdList.Add(pfd);
                }

                currentShoppingCart.TotalPrice = SetTotalPrice(currentShoppingCart);

                scfd = new ShoppingCartFormData();
                scfd.ProductQuantities = currentShoppingCart.ProductQuantities;
                scfd.Categories = categoryRepository.GetAll();
                scfd.ProductFormDatas = pfdList;
                scfd.ShoppingCart = currentShoppingCart;
                //}
                //else//Guestler için
                //{

                //}
                return View(scfd);

            }
            else
            {
                return View(new ShoppingCartFormData { Categories = categoryRepository.GetAll() });
            }


        }

        [HttpPost]
        public ActionResult PurchaseShoppingCart([ModelBinder(typeof(PurchaseModelBinder))] object[] returnArray)
        {
            var address = returnArray[0] as Address;
            var doesExits = (bool)returnArray[1];
            User currentUser = Session["User"] as User;

            userRepository.Attach(currentUser);


            string purchaseString = FakeData.TextData.GetAlphaNumeric(20);

            if (purchaseRepository.GetAll().Any(w => w.PurchaseString == purchaseString))
            {
                while (!purchaseRepository.GetAll().Any(w => w.PurchaseString == purchaseString))
                {
                    purchaseString = FakeData.TextData.GetAlphaNumeric(20);
                }
            }

            if (doesExits)
            {
                addressRepository.Attach(address);
            }
            else
            {
                currentUser.Addresses.Add(address);
                addressRepository.Add(address);
            }

            Purchase newPurchase = new Purchase();
            newPurchase.Address = address;
            newPurchase.PurchaseString = purchaseString;
            newPurchase.User = currentUser;
            newPurchase.PurchaseDate = DateTime.Now;
            currentUser.Purchases.Add(newPurchase);
            
            foreach (var item in currentUser.ShoppingCart.ProductQuantities)
            {
                item.Product.SoldCount += item.Quantity;
                newPurchase.ProductQuantities.Add(item);
            }

            newPurchase.TotalPrice = currentUser.ShoppingCart.TotalPrice;

            purchaseRepository.Add(newPurchase);

            try
            {
                uow.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("PurchasedPage", new { purchaseID = newPurchase.ID });
        }

        [HttpPost]
        public ActionResult DeleteItemFromCart(int ID)
        {
            ShoppingCart currentShoppingCart = (Session["User"] as User).ShoppingCart;

            Product deletedProduct = productRepository.GetById(ID);

            ProductQuantity prodQuantity = currentShoppingCart.ProductQuantities.SingleOrDefault(w => w.ProductID == ID);

            if (prodQuantity.Quantity == 1 || prodQuantity.Quantity == 0)
            {
                currentShoppingCart.ProductQuantities.Remove(prodQuantity);
                productQuantityRepository.Delete(prodQuantity);
            }
            else
            {
                prodQuantity.Quantity--;
            }
            currentShoppingCart.TotalPrice = SetTotalPrice(currentShoppingCart);

            uow.SaveChanges();

            return RedirectToAction("PurchaseShoppingCart", new { ID = currentShoppingCart.ID });
        }

        public ActionResult PurchasedPage(int purchaseID)
        {
            User currentUser = Session["User"] as User;
            ShoppingCart currentShoppingCart = currentUser.ShoppingCart;

            try
            {
                for (int i = 0; i < currentShoppingCart.ProductQuantities.Count; i++)
                {
                    currentShoppingCart.ProductQuantities.Remove(currentShoppingCart.ProductQuantities.ElementAt(i));
                }
                currentShoppingCart.TotalPrice = 0;
                uow.SaveChanges();
            }
            catch (Exception ex) { }

            Purchase newPurchase = purchaseRepository.GetById(purchaseID);

            PurchasedPageFormData ppfd = new PurchasedPageFormData();


            ppfd.Categories = categoryRepository.GetAll();
            ppfd.Purchase = newPurchase;
            return View(ppfd);
        }

        public ActionResult Deneme()
        {
            
            return View();
        }

        public ActionResult GetAllPurchases()
        {
            User currentUser = Session["User"] as User;
            ICollection<Purchase> allPurchasesList = currentUser.Purchases;

            GetAllPurchasesFormData gapfd = new GetAllPurchasesFormData();
            gapfd.Categories = categoryRepository.GetAll();
            gapfd.Purchases = allPurchasesList;

            return View(gapfd);
        }

        [HttpPost]
        public JsonResult Search(string name)
        {
            ICollection<Product> FilteredProducts = productRepository.GetBy(w => w.Name == name).ToList();
            
            return Json(FilteredProducts);
        }
        
    }
}