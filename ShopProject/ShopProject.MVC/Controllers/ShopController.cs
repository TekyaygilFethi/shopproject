﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.Filters;
using ShopProject.MVC.FormDatas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.Controllers
{
    
    public class ShopController : BaseController
    {
        IRepository<Category> categoryRepository = uow.GetRepository<Category>();
        IRepository<Product> productRepository = uow.GetRepository<Product>();

        public ActionResult Index()
        {
            ShopIndexFormData sifd = new ShopIndexFormData();

            ICollection<Category> Categories = categoryRepository.GetAll().ToList();
            ICollection<Product> AllProducts = productRepository.GetAll().ToList();

            ICollection<ProductFormData> pfdList = new List<ProductFormData>();
            ICollection<Product> finalizedProducts = new List<Product>();

            for (int i = 0; i < 8; i++) //get 4 random if there are 4 else return all existed
            {
                if (AllProducts.Count() != 0)
                {
                    Product randSelectedProduct = AllProducts.ElementAt(FakeData.NumberData.GetNumber(0, AllProducts.Count() - 1));

                    if (randSelectedProduct != null)
                    {
                        finalizedProducts.Add(randSelectedProduct);
                        AllProducts.Remove(randSelectedProduct);
                    }
                }
                else
                    break;
            }

            foreach (var production in finalizedProducts)
            {
                ProductFormData pfd = new ProductFormData();
                pfd.ID = production.ID;
                pfd.Base64Image = "data:image/png;base64," + Convert.ToBase64String(production.ProductImage);
                pfdList.Add(pfd);
            }

            

            #region Get Random Products by Random Category 
            //foreach (var category in )
            //{

            //    for (int i = 0; i < 8; i++)
            //    {
            //        if (ProductsOfCategory.Count() != 0)
            //        {
            //            Product randProduct = ProductsOfCategory.ElementAt(FakeData.NumberData.GetNumber(0, ProductsOfCategory.Count() - 1));


            //            if (randProduct != null)
            //            {
            //                ProductFormData productFormData = new ProductFormData();
            //                productFormData.ID = randProduct.ID;
            //                productFormData.Base64Image = "data:image/png;base64," + Convert.ToBase64String(randProduct.ProductImage);
            //                productFormDataList.Add(productFormData);
            //                randProductsByCatFM.Products.Add(randProduct);
            //                ProductsOfCategory.Remove(randProduct);
            //            }
            //            else
            //                break;
            //        }
            //        else
            //            break;
            //    }

            //    randProdByCatList.Add(randProductsByCatFM);
            //}
            #endregion

            sifd.Categories = categoryRepository.GetAll();
            sifd.RandomProducts = finalizedProducts;
            sifd.ProductFormDatas = pfdList;

            return View(sifd);
        }


    }
}