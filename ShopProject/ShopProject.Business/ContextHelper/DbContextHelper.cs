﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ShopProject.Data.DbContextFolder;

namespace ShopProject.Business.ContextHelper
{
    public class DbContextHelper : IDbContextHelper
    {
        private const string contextKey = "ShopProjectDb";
        public void DisposeContext()
        {
            if (HttpContext.Current.Items[contextKey] != null)
            {
                var context = (ShopProjectDbContext)HttpContext.Current.Items[contextKey];
                context.Dispose();
            }
        }

        public ShopProjectDbContext GetContext()
        {
            if (HttpContext.Current.Items[contextKey] == null)
            {
                HttpContext.Current.Items.Add(contextKey, new ShopProjectDbContext());
            }
             return (ShopProjectDbContext)HttpContext.Current.Items[contextKey];
        }
    }
}
