﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class CategorySaleFormData
    {
        public string CategoryName { get; set; }

        public int SoldCount { get; set; }
    }
}