namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserTable", "Salt", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserTable", "Salt", c => c.String(nullable: false));
        }
    }
}
