﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.DbContextFolder
{
    public class ShopProjectDbContext : DbContext
    {
        public ShopProjectDbContext() : base("ShopProjectDatabase") { }


        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<ProductQuantity> ProductQuantities { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<User>()
            //    .HasOptional(o => o.ShoppingCart)
            //    .WithRequired()
            //    .WillCascadeOnDelete(true);

            modelBuilder.Entity<User>()
                .HasMany(m => m.Addresses)
                .WithRequired(r => r.User)
                .HasForeignKey(fk => fk.UserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasOptional(o => o.ShoppingCart)
                .WithRequired()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<User>()
                .HasMany(o => o.Purchases)
                .WithRequired(r => r.User)
                .HasForeignKey(fk=>fk.UserID)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Review>()
                .HasRequired(r => r.Product)
                .WithMany(m => m.Reviews)
                .HasForeignKey(fk => fk.ProductID)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Review>()
                .HasRequired(r => r.User)
                .WithMany(o => o.Reviews)
                .HasForeignKey(fk => fk.UserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasRequired(r => r.Category)
                .WithMany(m => m.Products)
                .HasForeignKey(fk => fk.CategoryID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(m => m.ProductQuantities)
                .WithRequired(o => o.Product)
                .HasForeignKey(fk => fk.ProductID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Purchase>()
                .HasMany(m => m.ProductQuantities)
                .WithOptional()
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<Purchase>()
                .HasRequired(r => r.Address)
                .WithMany()
                .HasForeignKey(fk => fk.AddressID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductQuantity>()
                .HasOptional(r => r.ShoppingCart)
                .WithMany(m => m.ProductQuantities)
                .HasForeignKey(fk => fk.ShoppingCartID)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Category>()
            //    .HasMany(m => m.SubCategories)
            //    .WithOptional(o => o.ParentCategory)
            //    .HasForeignKey(fk => fk.CategoryID)
            //    .WillCascadeOnDelete(false);
        }
    }
}
