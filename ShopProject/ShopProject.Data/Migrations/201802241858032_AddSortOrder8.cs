namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder8 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ShoppingCartTable", name: "UserID", newName: "ID");
            RenameIndex(table: "dbo.ShoppingCartTable", name: "IX_UserID", newName: "IX_ID");
            AddForeignKey("dbo.ShoppingCartTable", "ID", "dbo.UserTable", "ID");
            DropColumn("dbo.UserTable", "ShoppingCartID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserTable", "ShoppingCartID", c => c.Int());
            DropForeignKey("dbo.ShoppingCartTable", "ID", "dbo.UserTable");
            RenameIndex(table: "dbo.ShoppingCartTable", name: "IX_ID", newName: "IX_UserID");
            RenameColumn(table: "dbo.ShoppingCartTable", name: "ID", newName: "UserID");
        }
    }
}
