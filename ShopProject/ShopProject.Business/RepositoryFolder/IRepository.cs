﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Business.RepositoryFolder
{
    public interface IRepository<T> where T:class
    {
        void Add(T _item);
        void Update(int ID,T _newItem);
        void Delete(T _deletedItem);
        void DeleteMany(IEnumerable<T> _deletedList);
        void Attach(T _item);
        bool IsAny(int ID);
        T GetById(int ID);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetBy(Func<T, bool> predicate);
        T SingleGetBy(Expression<Func<T, bool>> predicate);
    }
}
