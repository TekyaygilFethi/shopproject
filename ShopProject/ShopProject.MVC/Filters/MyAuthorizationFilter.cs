﻿using Newtonsoft.Json;
using ShopProject.Security;
using ShopProject.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ShopProject.MVC.Filters
{
    public class MyAuthorizationFilter : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if(actionContext.Request.Headers.Authorization==null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                LogonUser logonUser = null;

                try
                {
                    var tokenKey = actionContext.Request.Headers.Authorization.Parameter;
                    var jsonString = FTH.Extension.Encrypter.Decrypt(tokenKey, AccountSecurity.TokenPassword);
                    logonUser = JsonConvert.DeserializeObject<LogonUser>(jsonString);
                }
                catch 
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    return;
                }

                if(AccountSecurity.IsUserExists(logonUser.Email,logonUser.Password))
                {
                    Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(logonUser.Email), null);
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                }

            }
        }
    }
}