﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class ViewAllUsersFormData
    {
        public string Usermail { get; set; }

        public string Username { get; set; }

        public int PurchaseCount { get; set; }

        public DateTime LastShopping { get; set; }
    }
}