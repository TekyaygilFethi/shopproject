namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder7 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserTable", "Surname");
            DropColumn("dbo.UserTable", "Birthday");
            DropColumn("dbo.UserTable", "PhoneNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserTable", "PhoneNumber", c => c.String(nullable: false));
            AddColumn("dbo.UserTable", "Birthday", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserTable", "Surname", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
