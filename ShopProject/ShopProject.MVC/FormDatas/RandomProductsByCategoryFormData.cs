﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class RandomProductsByCategoryFormData
    {
        public RandomProductsByCategoryFormData()
        {
            Products = new List<Product>();
        }
        public int CategoryID { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}