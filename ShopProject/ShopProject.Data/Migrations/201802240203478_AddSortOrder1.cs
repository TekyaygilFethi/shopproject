namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryTable", "CategoryID", c => c.Int());
            CreateIndex("dbo.CategoryTable", "CategoryID");
            AddForeignKey("dbo.CategoryTable", "CategoryID", "dbo.CategoryTable", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryTable", "CategoryID", "dbo.CategoryTable");
            DropIndex("dbo.CategoryTable", new[] { "CategoryID" });
            DropColumn("dbo.CategoryTable", "CategoryID");
        }
    }
}
