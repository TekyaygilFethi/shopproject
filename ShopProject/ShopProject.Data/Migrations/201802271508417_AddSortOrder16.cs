namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder16 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseTable", "TotalPrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseTable", "TotalPrice");
        }
    }
}
