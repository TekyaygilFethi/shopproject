namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder10 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserTable", "Salt", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserTable", "Salt", c => c.String());
        }
    }
}
