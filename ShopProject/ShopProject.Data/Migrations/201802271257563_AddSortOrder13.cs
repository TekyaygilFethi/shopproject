namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseProductTable", "PurchaseID", "dbo.PurchaseTable");
            DropForeignKey("dbo.PurchaseProductTable", "ProductID", "dbo.ProductTable");
            DropIndex("dbo.PurchaseProductTable", new[] { "PurchaseID" });
            DropIndex("dbo.PurchaseProductTable", new[] { "ProductID" });
            CreateTable(
                "dbo.ProductQuantities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PurchaseID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PurchaseTable", t => t.PurchaseID)
                .ForeignKey("dbo.ProductTable", t => t.ProductID)
                .Index(t => t.PurchaseID)
                .Index(t => t.ProductID);
            
            DropTable("dbo.PurchaseProductTable");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PurchaseProductTable",
                c => new
                    {
                        PurchaseID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PurchaseID, t.ProductID });
            
            DropForeignKey("dbo.ProductQuantities", "ProductID", "dbo.ProductTable");
            DropForeignKey("dbo.ProductQuantities", "PurchaseID", "dbo.PurchaseTable");
            DropIndex("dbo.ProductQuantities", new[] { "ProductID" });
            DropIndex("dbo.ProductQuantities", new[] { "PurchaseID" });
            DropTable("dbo.ProductQuantities");
            CreateIndex("dbo.PurchaseProductTable", "ProductID");
            CreateIndex("dbo.PurchaseProductTable", "PurchaseID");
            AddForeignKey("dbo.PurchaseProductTable", "ProductID", "dbo.ProductTable", "ID", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseProductTable", "PurchaseID", "dbo.PurchaseTable", "ID", cascadeDelete: true);
        }
    }
}
