﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class GetAllPurchasesFormData
    {
        public IEnumerable<Category> Categories { get; set; }

        public IEnumerable<Purchase> Purchases { get; set; }
    }
}