﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    public class ProductQuantity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public virtual ShoppingCart ShoppingCart { get; set; }

        public int? ShoppingCartID { get; set; }

        public virtual Product Product { get; set; }

        public int ProductID { get; set; }

        public int Quantity { get; set; }

        [NotMapped]
        public double GetTotalPrice { get { return Product.Price * Quantity; } }
    }
}
