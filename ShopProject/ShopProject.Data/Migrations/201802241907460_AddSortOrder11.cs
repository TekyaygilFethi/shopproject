namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserTable", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserTable", "Password", c => c.String(nullable: false, maxLength: 30));
        }
    }
}
