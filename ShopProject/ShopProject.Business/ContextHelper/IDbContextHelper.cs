﻿using ShopProject.Data.DbContextFolder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Business.ContextHelper
{
    interface IDbContextHelper
    {
        ShopProjectDbContext GetContext();
        void DisposeContext();
    }
}
