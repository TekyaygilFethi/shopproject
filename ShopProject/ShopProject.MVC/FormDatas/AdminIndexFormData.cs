﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class AdminIndexFormData
    {
        public ICollection<User> Users { get; set; }

        public ICollection<Product> Products { get; set; }

        public ICollection<Product> TopSoldProducts { get; set; }

        public ICollection<Category> Categories { get; set; }
        
        public ICollection<Purchase> Purchases { get; set; }

        public ICollection<CategorySaleFormData> CategorySaleFormDatas { get; set; }

        public double TotalMoney { get; set; }



    }
}