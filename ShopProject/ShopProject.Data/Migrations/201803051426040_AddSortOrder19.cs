namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseTable", "PurchaseDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseTable", "PurchaseDate");
        }
    }
}
