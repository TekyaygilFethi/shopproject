namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MailVerificationTable", "UserID", "dbo.UserTable");
            DropIndex("dbo.MailVerificationTable", new[] { "UserID" });
            DropColumn("dbo.UserTable", "IsVerified");
            DropTable("dbo.MailVerificationTable");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MailVerificationTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        VerificationCode = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.UserTable", "IsVerified", c => c.Boolean(nullable: false));
            CreateIndex("dbo.MailVerificationTable", "UserID");
            AddForeignKey("dbo.MailVerificationTable", "UserID", "dbo.UserTable", "ID", cascadeDelete: true);
        }
    }
}
