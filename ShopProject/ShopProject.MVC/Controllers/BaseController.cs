﻿using ShopProject.Business.ContextHelper;
using ShopProject.Business.UnitOfWorkFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.Controllers
{
    public class BaseController : Controller,IDisposable
    {
        static private DbContextHelper contextHelper = new DbContextHelper();
        static public UnitOfWork uow = new UnitOfWork(contextHelper.GetContext());
    }
}