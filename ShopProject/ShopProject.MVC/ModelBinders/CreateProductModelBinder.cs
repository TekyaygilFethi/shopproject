﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.ModelBinders
{
    public class CreateProductModelBinder : BaseController, IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            IRepository<Category> categoryRepository = uow.GetRepository<Category>();
            HttpRequestBase request = controllerContext.HttpContext.Request;

            var name = request.Form.Get("Name");
            var price = request.Form.Get("Price");
            var size = request.Form.Get("Size");
            var image = request.Form.Get("ImageBase64");
            var categoryID = request.Form.Get("CategoryID");
            var category = categoryRepository.GetById(int.Parse(categoryID));

            categoryRepository.Attach(category);

            Product newProduct = new Product();
            newProduct.Name = name;
            newProduct.Price = double.Parse(price);
            newProduct.Size = (Size)int.Parse(size);
            newProduct.ProductImage = ImageOpsFolder.ImageOps.ConvertToByteArray(image);

            newProduct.Category = category;
            category.Products.Add(newProduct);





            //object[] dataArray = new object[2];

            //dataArray[0] = newProduct;
            //dataArray[1] = category;
            return newProduct;



        }
    }
}