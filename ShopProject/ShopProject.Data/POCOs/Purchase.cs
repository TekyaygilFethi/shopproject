﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    [Table("PurchaseTable")]
    public class Purchase
    {
        public Purchase()
        {
            ProductQuantities = new List<ProductQuantity>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(30)]
        [Required]
        public string PurchaseString { get; set; }
        
        public virtual ICollection<ProductQuantity> ProductQuantities { get; set; }
        
        public virtual Address Address { get; set; }

        public int AddressID { get; set; }
        
        public virtual User User { get; set; }

        public int UserID { get; set; }

        public double TotalPrice { get; set; }

        public DateTime PurchaseDate { get; set; }

    }
}
