﻿
using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.FormDatas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopProject.MVC.Filters;

namespace ShopProject.MVC.Controllers
{
    [AdminRoleAuthentication]
    public class AdminController : BaseController
    {
        IRepository<User> userRepository = uow.GetRepository<User>();
        IRepository<Product> productRepository = uow.GetRepository<Product>();
        IRepository<Category> categoryRepository = uow.GetRepository<Category>();
        IRepository<Purchase> purchaseRepository = uow.GetRepository<Purchase>();

        // GET: Admin
        public ActionResult Index()
        {
            AdminIndexFormData aifd = new AdminIndexFormData();
            aifd.Categories = categoryRepository.GetAll().ToList();
            aifd.Products = productRepository.GetAll().ToList();
            aifd.Purchases = purchaseRepository.GetAll().ToList();
            aifd.Users = userRepository.GetAll().ToList();

            ICollection<CategorySaleFormData> csfdList = new List<CategorySaleFormData>();

            foreach (var category in categoryRepository.GetAll())
            {
                foreach (var product in category.Products)
                {
                    CategorySaleFormData csfd = new CategorySaleFormData();
                    csfd.CategoryName = category.Name;
                    csfd.SoldCount += product.SoldCount;

                    csfdList.Add(csfd);
                }
            }

            aifd.CategorySaleFormDatas = csfdList;

            double price = 0;
            foreach (var v in purchaseRepository.GetAll().Select(s => s.TotalPrice))
            {
                price += v;
            }
            aifd.TotalMoney = price;

            aifd.TopSoldProducts = productRepository.GetAll().OrderBy(w => w.SoldCount).Take(15).ToList();

            return View(aifd);
        }

        public ActionResult Index2()
        {
            //foreach(var user in userRepository.GetAll())
            //{
            //    foreach(var purchase in user.Purchases)
            //    {

            //    }
            //}


            return View();
        }

        public ActionResult ProductSoldCountChartPartial()
        {
            return PartialView("_ProductSoldCountChartPartial", productRepository.GetAll().OrderBy(w => w.SoldCount).ToList());
        }

        public ActionResult CategorySaleChartPartial()
        {
            ICollection<CategorySaleFormData> csfdList = new List<CategorySaleFormData>();

            foreach (var category in categoryRepository.GetAll())
            {
                CategorySaleFormData csfd = new CategorySaleFormData();
                csfd.CategoryName = category.Name;
                foreach (var product in category.Products)
                {

                    csfd.SoldCount += product.SoldCount;


                }
                csfdList.Add(csfd);
            }

            return PartialView("_CategorySaleChartPartial", csfdList.OrderBy(o => o.SoldCount).ToList());
        }

        public ActionResult UserPurchasesChartPartial()
        {
            ICollection<UserPurchaseFormData> upfdList = new List<UserPurchaseFormData>();



            foreach (var user in userRepository.GetAll())
            {
                UserPurchaseFormData upfd = new UserPurchaseFormData();
                upfd.Username = user.Name;
                upfd.Usermail = user.Email;

                foreach (var purchase in user.Purchases)
                {
                    foreach (var prodQuantity in purchase.ProductQuantities)
                    {
                        upfd.PurchaseCount += prodQuantity.Quantity;
                    }
                    upfd.PurchaseCount += purchase.ProductQuantities.Count();
                }
                upfdList.Add(upfd);
            }


            return PartialView("_UserPurchasesChartPartial", upfdList.OrderBy(o=>o.PurchaseCount).Take(5).ToList());
        }
        
        public ActionResult ViewAllUsers()
        {
            ICollection<ViewAllUsersFormData> vaufdList = new List<ViewAllUsersFormData>();

            foreach (var user in userRepository.GetAll())
            {
                ViewAllUsersFormData vaufd = new ViewAllUsersFormData();
                vaufd.LastShopping = (user.Purchases != null && user.Purchases.Count != 0) ? user.Purchases.OrderByDescending(b => b.PurchaseDate).FirstOrDefault().PurchaseDate : new DateTime();
                vaufd.PurchaseCount = user.Purchases.Count();
                vaufd.Usermail = user.Email;
                vaufd.Username = user.Name;

                vaufdList.Add(vaufd);
            }

            return View(vaufdList);
        }

        public ActionResult ViewAllPurchases()
        {
            return View(purchaseRepository.GetAll());
        }

        public PartialViewResult ViewPurchaseProducts(int ID)
        {
            Purchase purchase = purchaseRepository.GetById(ID);

            TempData["PurchaseString"] = purchase.PurchaseString;

            return PartialView(purchase.ProductQuantities);
        }

        public ActionResult ViewAllCategories()
        {
            return View(categoryRepository.GetAll());
        }

        public PartialViewResult ViewCategoryProducts(int ID)
        {
            Category category = categoryRepository.GetById(ID);
            
            return PartialView(category.Products);
        }

        public ActionResult ViewProducts()
        {
            return View(productRepository.GetAll());
        }
        
        public ActionResult PurchasesByDateChartPartial()
        {
            var model = purchaseRepository.GetAll();
            return PartialView("_PurchasesByDateChartPartial", model);
        }
    }
}