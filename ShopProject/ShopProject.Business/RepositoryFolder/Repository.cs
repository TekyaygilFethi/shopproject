﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ShopProject.Data.DbContextFolder;
using System.Data.Entity;

namespace ShopProject.Business.RepositoryFolder
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public ShopProjectDbContext db;

        public Repository(ShopProjectDbContext _context)
        {
            db = _context;
        }

        public void Add(T item)
        {
            db.Set<T>().Add(item);
        }

        public void Attach(T item)
        {
            db.Set<T>().Attach(item);
        }

        public void Delete(T deletedItem)
        {
           db.Set<T>().Remove(deletedItem);
        }

        public void DeleteMany(IEnumerable<T> deletedList)
        {
            db.Set<T>().RemoveRange(deletedList);
        }

        public IEnumerable<T> GetAll()
        {
            return db.Set<T>().ToList();
        }

        public  IEnumerable<T> GetBy(Func<T, bool> predicate)
        {
            return  db.Set<T>().Where(predicate);
        }

        public  T GetById(int ID)
        {
            return db.Set<T>().Find(ID);
        }

        public bool IsAny(int ID)
        {
            T item = GetById(ID);
            if (item != null)
                return true;
            else
                return false;
        }

        public T SingleGetBy(Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>().SingleOrDefault(predicate);
        }

        public void Update(int id,T item)
        {
           db.Entry<T>(item).State = EntityState.Modified;
        }
        
    }
}
