namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder15 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseTable", "ID", "dbo.ShoppingCartTable");
            DropIndex("dbo.PurchaseTable", new[] { "ID" });
            AddColumn("dbo.ProductQuantities", "Purchase_ID", c => c.Int());
            CreateIndex("dbo.ProductQuantities", "Purchase_ID");
            CreateIndex("dbo.ShoppingCartTable", "PurchaseID");
            AddForeignKey("dbo.ProductQuantities", "Purchase_ID", "dbo.PurchaseTable", "ID");
            AddForeignKey("dbo.ShoppingCartTable", "PurchaseID", "dbo.PurchaseTable", "ID");
            DropColumn("dbo.PurchaseTable", "ShoppingCartID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseTable", "ShoppingCartID", c => c.Int(nullable: false));
            DropForeignKey("dbo.ShoppingCartTable", "PurchaseID", "dbo.PurchaseTable");
            DropForeignKey("dbo.ProductQuantities", "Purchase_ID", "dbo.PurchaseTable");
            DropIndex("dbo.ShoppingCartTable", new[] { "PurchaseID" });
            DropIndex("dbo.ProductQuantities", new[] { "Purchase_ID" });
            DropColumn("dbo.ProductQuantities", "Purchase_ID");
            CreateIndex("dbo.PurchaseTable", "ID");
            AddForeignKey("dbo.PurchaseTable", "ID", "dbo.ShoppingCartTable", "ID");
        }
    }
}
