﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    [Table("CategoryTable")]
    public class Category
    {
        public Category()
        {
            Products = new List<Product>();
            //SubCategories = new List<Category>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(100,MinimumLength =1,ErrorMessage ="Category name must contains 1-100 characters")]
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        //public virtual ICollection<Category> SubCategories { get; set; }

        //public virtual Category ParentCategory { get; set; }

        //public int? CategoryID { get; set; }
    }
}
