namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder18 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ProductQuantities", new[] { "ShoppingCartID" });
            AlterColumn("dbo.ProductQuantities", "ShoppingCartID", c => c.Int());
            CreateIndex("dbo.ProductQuantities", "ShoppingCartID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ProductQuantities", new[] { "ShoppingCartID" });
            AlterColumn("dbo.ProductQuantities", "ShoppingCartID", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductQuantities", "ShoppingCartID");
        }
    }
}
