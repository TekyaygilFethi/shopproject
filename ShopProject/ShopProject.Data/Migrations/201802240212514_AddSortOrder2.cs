namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductTable", "ProductImage", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductTable", "ProductImage");
        }
    }
}
