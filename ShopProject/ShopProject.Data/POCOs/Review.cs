﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    [Table("ReviewTable")]
    public class Review
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DataType(DataType.MultilineText)]
        public string ReviewString { get; set; }

        [Required]
        [ForeignKey("UserID")]
        public virtual User User { get; set; }

        [Required]
        public int UserID { get; set; }

        [Required]
        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [Required]
        public int ProductID { get; set; }
        
    }
}
