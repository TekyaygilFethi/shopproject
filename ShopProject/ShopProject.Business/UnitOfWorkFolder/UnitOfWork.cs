﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.DbContextFolder;


namespace ShopProject.Business.UnitOfWorkFolder
{
    public class UnitOfWork : IUnitOfWork,IDisposable
    {
        private readonly ShopProjectDbContext dbContext;

        public UnitOfWork(ShopProjectDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            return new Repository<T>(dbContext);
        }

        public int SaveChanges()
        {
            return  dbContext.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }

            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
