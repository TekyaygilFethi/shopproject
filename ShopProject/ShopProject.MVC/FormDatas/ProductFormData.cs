﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class ProductFormData
    {
        public int ID { get; set; }

        public string Base64Image { get; set; }
    }
}