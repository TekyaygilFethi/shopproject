namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserTable", "Email", c => c.String(nullable: false, maxLength: 30));
            CreateIndex("dbo.UserTable", "Email", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserTable", new[] { "Email" });
            AlterColumn("dbo.UserTable", "Email", c => c.String(nullable: false));
        }
    }
}
