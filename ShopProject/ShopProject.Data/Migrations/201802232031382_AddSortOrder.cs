namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Surname = c.String(nullable: false, maxLength: 20),
                        Email = c.String(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                        Password = c.String(nullable: false, maxLength: 30),
                        Salt = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        IsVerified = c.Boolean(nullable: false),
                        ProfilePhoto = c.Binary(),
                        ShoppingCartID = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Price = c.Double(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        Size = c.Int(nullable: false),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CategoryTable", t => t.CategoryID)
                .ForeignKey("dbo.UserTable", t => t.User_ID)
                .Index(t => t.CategoryID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.CategoryTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ReviewTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ReviewString = c.String(),
                        UserID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProductTable", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.UserTable", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.ShoppingCartTable",
                c => new
                    {
                        UserID = c.Int(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShoppingCartTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.ProductTable", "User_ID", "dbo.UserTable");
            DropForeignKey("dbo.ReviewTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.ReviewTable", "ProductID", "dbo.ProductTable");
            DropForeignKey("dbo.ProductTable", "CategoryID", "dbo.CategoryTable");
            DropIndex("dbo.ShoppingCartTable", new[] { "UserID" });
            DropIndex("dbo.ReviewTable", new[] { "ProductID" });
            DropIndex("dbo.ReviewTable", new[] { "UserID" });
            DropIndex("dbo.ProductTable", new[] { "User_ID" });
            DropIndex("dbo.ProductTable", new[] { "CategoryID" });
            DropTable("dbo.ShoppingCartTable");
            DropTable("dbo.ReviewTable");
            DropTable("dbo.CategoryTable");
            DropTable("dbo.ProductTable");
            DropTable("dbo.UserTable");
        }
    }
}
