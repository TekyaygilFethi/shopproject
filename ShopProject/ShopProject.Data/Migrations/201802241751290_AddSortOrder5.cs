namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AddressTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        AddressInfo = c.String(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.PurchaseTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PurchaseString = c.String(nullable: false, maxLength: 30),
                        ShoppingCartID = c.Int(nullable: false),
                        AddressID = c.Int(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AddressTable", t => t.AddressID)
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .Index(t => t.PurchaseString, unique: true)
                .Index(t => t.AddressID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.PurchaseProductTable",
                c => new
                    {
                        PurchaseID = c.Int(nullable: false),
                        ProductID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PurchaseID, t.ProductID })
                .ForeignKey("dbo.PurchaseTable", t => t.PurchaseID, cascadeDelete: true)
                .ForeignKey("dbo.ProductTable", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.PurchaseID)
                .Index(t => t.ProductID);
            
            AddColumn("dbo.ProductTable", "Description", c => c.String());
            AddColumn("dbo.UserTable", "Role", c => c.Int(nullable: false));
            DropColumn("dbo.UserTable", "ProfilePhoto");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserTable", "ProfilePhoto", c => c.Binary());
            DropForeignKey("dbo.PurchaseTable", "UserID", "dbo.UserTable");
            DropForeignKey("dbo.PurchaseProductTable", "ProductID", "dbo.ProductTable");
            DropForeignKey("dbo.PurchaseProductTable", "PurchaseID", "dbo.PurchaseTable");
            DropForeignKey("dbo.PurchaseTable", "AddressID", "dbo.AddressTable");
            DropForeignKey("dbo.AddressTable", "UserID", "dbo.UserTable");
            DropIndex("dbo.PurchaseProductTable", new[] { "ProductID" });
            DropIndex("dbo.PurchaseProductTable", new[] { "PurchaseID" });
            DropIndex("dbo.PurchaseTable", new[] { "UserID" });
            DropIndex("dbo.PurchaseTable", new[] { "AddressID" });
            DropIndex("dbo.PurchaseTable", new[] { "PurchaseString" });
            DropIndex("dbo.AddressTable", new[] { "UserID" });
            DropColumn("dbo.UserTable", "Role");
            DropColumn("dbo.ProductTable", "Description");
            DropTable("dbo.PurchaseProductTable");
            DropTable("dbo.PurchaseTable");
            DropTable("dbo.AddressTable");
        }
    }
}
