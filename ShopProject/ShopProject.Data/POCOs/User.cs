﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    public enum Role
    {
        Basic,
        Admin
    }

    
    [Table("UserTable")]
    public class User
    {
        public User()
        {
            FavouriteProducts = new List<Product>();
            Addresses = new List<Address>();
            Purchases = new List<Purchase>();
            Salt = "";
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(20,MinimumLength =1,ErrorMessage ="Name field must contain 1-20 characters!")]
        public string Name { get; set; }
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Index(IsUnique =true)]
        [MaxLength(30)]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]        
        public string Salt { get; set; }
        
        [Required]
        public Role Role { get; set; }

        public virtual ShoppingCart ShoppingCart { get; set; }

        //public int? ShoppingCartID { get; set; }

        public virtual ICollection<Product> FavouriteProducts { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }

        public virtual ICollection<Purchase> Purchases { get; set; }
        
    }
}
