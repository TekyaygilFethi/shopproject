﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.ModelBinders
{
    public class PurchaseModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            bool doesExists = false;
            int ID = -1;
            
            try
            {
                ID = int.Parse(request.Form.Get("AddrID"));
            }catch(Exception ex) {  }
            var currentUser= controllerContext.HttpContext.Session["User"] as User;
            object[] returnArray = new object[2];


            if(currentUser.Addresses.SingleOrDefault(w=>w.ID==ID)!=null)
            {
                returnArray[0]=currentUser.Addresses.SingleOrDefault(w => w.ID == ID);
                doesExists = true;
                returnArray[1] = doesExists;
                return returnArray;
            }
            else
            {
                var address = request.Form.Get("Addr");
                var addressInfo = request.Form.Get("AddressInf");

                Address newAddress = new Address();
                newAddress.AddressInfo = addressInfo;
                newAddress.Description = address;
                newAddress.User = currentUser;

                returnArray[0] = newAddress;
                returnArray[1] = doesExists;

                return returnArray;
            }
        }
    }
}