﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class ShopIndexFormData
    {
        public IEnumerable<Category> Categories { get; set; }

        public ICollection<Product> RandomProducts { get; set; }

        public IEnumerable<ProductFormData> ProductFormDatas { get; set; }
    }
}