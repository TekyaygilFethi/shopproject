// <auto-generated />
namespace ShopProject.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddSortOrder9 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddSortOrder9));
        
        string IMigrationMetadata.Id
        {
            get { return "201802241903038_AddSortOrder9"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
