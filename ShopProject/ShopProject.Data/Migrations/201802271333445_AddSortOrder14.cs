namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder14 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductQuantities", "PurchaseID", "dbo.PurchaseTable");
            DropIndex("dbo.ProductQuantities", new[] { "PurchaseID" });
            AddColumn("dbo.ProductQuantities", "ShoppingCartID", c => c.Int(nullable: false));
            AddColumn("dbo.ShoppingCartTable", "PurchaseID", c => c.Int());
            CreateIndex("dbo.ProductQuantities", "ShoppingCartID");
            CreateIndex("dbo.PurchaseTable", "ID");
            AddForeignKey("dbo.PurchaseTable", "ID", "dbo.ShoppingCartTable", "ID");
            AddForeignKey("dbo.ProductQuantities", "ShoppingCartID", "dbo.ShoppingCartTable", "ID");
            DropColumn("dbo.ProductQuantities", "PurchaseID");
            DropColumn("dbo.PurchaseTable", "TotalPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PurchaseTable", "TotalPrice", c => c.Double(nullable: false));
            AddColumn("dbo.ProductQuantities", "PurchaseID", c => c.Int(nullable: false));
            DropForeignKey("dbo.ProductQuantities", "ShoppingCartID", "dbo.ShoppingCartTable");
            DropForeignKey("dbo.PurchaseTable", "ID", "dbo.ShoppingCartTable");
            DropIndex("dbo.PurchaseTable", new[] { "ID" });
            DropIndex("dbo.ProductQuantities", new[] { "ShoppingCartID" });
            DropColumn("dbo.ShoppingCartTable", "PurchaseID");
            DropColumn("dbo.ProductQuantities", "ShoppingCartID");
            CreateIndex("dbo.ProductQuantities", "PurchaseID");
            AddForeignKey("dbo.ProductQuantities", "PurchaseID", "dbo.PurchaseTable", "ID");
        }
    }
}
