﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.ImageOpsFolder
{
    public class ImageOps
    {
        #region IMAGE OPS
        public static string ConvertToBase64(byte[] Image)
        {
            return Convert.ToBase64String(Image);
        }

        public static byte[] ConvertToByteArray(string Base64Text)
        {
            return Convert.FromBase64String(Base64Text);
        }
        #endregion
    }
}