﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using ShopProject.MVC.ModelBinders;
using ShopProject.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ShopProject.MVC.Controllers
{
    public class UserController : BaseController
    {
        IRepository<User> userRepository = uow.GetRepository<User>();
        IRepository<ShoppingCart> cartRepository = uow.GetRepository<ShoppingCart>();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login(string email, string password)
        {
            if (AccountSecurity.IsUserExists(email, password))
            {
                ShopProject.Data.POCOs.User user = userRepository.SingleGetBy(w => w.Email == email);
                //HttpContext.Session.Add("User",user);
                Session["User"] = user;
                return Json("Login Successed!");
            }
            else
            {
                return Json("Your e-mail or password is incorrect!");
            }


        }

        [HttpPost]
        public ActionResult CreateAccount([ModelBinder(typeof(CreateAccountModelBinder))]object[] userAndCart)
        {
            User user = userAndCart[0] as User;
            ShoppingCart cart = userAndCart[1] as ShoppingCart;

            userRepository.Add(user);
            cartRepository.Add(cart);
            try
            {
                uow.SaveChanges();
            }
            catch (Exception ex) { }
            return View();
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Shop");
        }
    }
}