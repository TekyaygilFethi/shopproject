﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    public enum Size
    {
        None,
        XS,
        M,
        L,
        XL
    }

    [Table("ProductTable")]
    public class Product
    {
        public Product()
        {
            Reviews = new List<Review>();
            ProductQuantities = new List<ProductQuantity>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Category name must contains 1-100 characters")]
        public string Name { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public string Description { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [Required]
        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }

        [Required]
        public int CategoryID { get; set; }

        public Size Size { get; set; }

        public byte[] ProductImage { get; set; }

        public int SoldCount { get; set; }

        public virtual ICollection<ProductQuantity> ProductQuantities { get; set; }
    }
}
