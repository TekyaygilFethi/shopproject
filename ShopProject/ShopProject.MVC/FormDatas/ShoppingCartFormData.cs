﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class ShoppingCartFormData
    {
        public IEnumerable<Category> Categories { get; set; }

        public IEnumerable<ProductQuantity> ProductQuantities { get; set; }

        public IEnumerable<ProductFormData> ProductFormDatas { get; set; }

        public ShoppingCart ShoppingCart { get; set; }
    }
}