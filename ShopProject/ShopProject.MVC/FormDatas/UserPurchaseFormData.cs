﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class UserPurchaseFormData
    {
        public string Username { get; set; }

        public string Usermail { get; set; }

        public int PurchaseCount { get; set; }
    }
}