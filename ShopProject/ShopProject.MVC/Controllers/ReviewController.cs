﻿using ShopProject.Business.RepositoryFolder;
using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.Controllers
{
    public class ReviewController : BaseController
    {
        IRepository<Review> reviewRepository = uow.GetRepository<Review>();
        IRepository<Product> productRepository = uow.GetRepository<Product>();
        IRepository<User> userRepository = uow.GetRepository<User>();

        [HttpPost]
        public JsonResult AddReview(string review,int productID)
        {
            User currentUser = Session["User"] as User;
            Product currentProduct = productRepository.GetById(productID);

            userRepository.Attach(currentUser);
            productRepository.Attach(currentProduct);
            
            Review newReview = new Review();
            newReview.Product = currentProduct;
            newReview.ReviewString = review;
            newReview.User = currentUser;

            currentUser.Reviews.Add(newReview);
            currentProduct.Reviews.Add(newReview);

            try
            {
                uow.SaveChanges();
            }catch(Exception ex)
            {

            }
            
            return Json("OK");
        }
    }
}