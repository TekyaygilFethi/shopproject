﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.Filters
{
    public class AdminRoleAuthentication : System.Web.Mvc.FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
       {
            if(filterContext.HttpContext.Session["User"]!=null && (filterContext.HttpContext.Session["User"] as User).Role!=Role.Admin)
            {
                filterContext.Result = new RedirectResult("../Shop/Index");
            }
            //if((filterContext.HttpContext.Session["User"] as User).Role==Role.Admin)
            //{
            //    filterContext.Result = new RedirectResult("../Admin/Index");
            //}
        }
    }
}