﻿using ShopProject.Data.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class ProductDetailsFormData
    {
        public IEnumerable<Category> Categories { get; set; }

        public Product Product { get; set; }

        public IEnumerable<ProductFormData> ProductFormDatas { get; set; }

        public IEnumerable<Product> SameCategoryProducts { get; set; }
    }
}