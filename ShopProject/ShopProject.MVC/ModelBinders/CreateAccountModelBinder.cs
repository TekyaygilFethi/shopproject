﻿using ShopProject.Data.POCOs;
using ShopProject.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopProject.MVC.ModelBinders
{
    public class CreateAccountModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            var name = request.Form.Get("Name");
            var password = request.Form.Get("Password");
            var email = request.Form.Get("Email");

            ShoppingCart cart = new ShoppingCart();

            User user = new User();
            user.Name = name;
            user.Salt = AccountSecurity.GenerateSalt();
            user.Password = AccountSecurity.GetHashSaltPassword(password,user.Salt);
            user.Role = Role.Basic;
            user.Email = email;
            
            user.ShoppingCart = cart;
            cart.User = user;

            object[] userAndCart = new object[2];
            userAndCart[0] = user;
            userAndCart[1] = cart;

            return userAndCart;
        }
    }
}