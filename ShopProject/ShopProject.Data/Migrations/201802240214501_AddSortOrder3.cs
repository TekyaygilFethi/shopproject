namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductTable", "SoldCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductTable", "SoldCount");
        }
    }
}
