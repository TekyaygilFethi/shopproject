namespace ShopProject.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrder4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CategoryTable", "CategoryID", "dbo.CategoryTable");
            DropIndex("dbo.CategoryTable", new[] { "CategoryID" });
            CreateTable(
                "dbo.MailVerificationTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        VerificationCode = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserTable", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            DropColumn("dbo.CategoryTable", "CategoryID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CategoryTable", "CategoryID", c => c.Int());
            DropForeignKey("dbo.MailVerificationTable", "UserID", "dbo.UserTable");
            DropIndex("dbo.MailVerificationTable", new[] { "UserID" });
            DropTable("dbo.MailVerificationTable");
            CreateIndex("dbo.CategoryTable", "CategoryID");
            AddForeignKey("dbo.CategoryTable", "CategoryID", "dbo.CategoryTable", "ID");
        }
    }
}
