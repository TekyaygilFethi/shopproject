﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopProject.MVC.FormDatas
{
    public class AddProductionFormData
    {
        public string Name { get; set; }

        public double Price { get; set; }

        public string Size { get; set; }

        public string ImageBase64 { get; set; }

        public string CategoryID { get; set; }
    }
}