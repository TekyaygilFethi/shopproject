﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopProject.Data.POCOs
{
    [Table("ShoppingCartTable")]
    public class ShoppingCart
    {
        public ShoppingCart()
        {
            ProductQuantities = new List<ProductQuantity>();
        }

        [Key]
        [ForeignKey("User")]
        public int ID { get; set; }

        public virtual ICollection<ProductQuantity> ProductQuantities { get; set; }

        public virtual User User { get; set; }

        public virtual Purchase Purchase { get; set; }

        public int? PurchaseID { get; set; }

        [DataType(DataType.Currency)]
        public double TotalPrice { get; set; }
    }
}
